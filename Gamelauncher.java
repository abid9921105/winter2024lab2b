import java.util.Scanner;
public class Gamelauncher{
	public static void main(String[] args){
    Scanner reader = new Scanner(System.in);
	
    System.out.println("Type 1 if you want to play Hangman or 2 if you want to play Wordle");
    int x = reader.nextInt();
	if (x == 1){
		System.out.println("Lauching Hangman... Please wait a moment");
		System.out.println("Gimme a four letter word pls!!!");
		String word = reader.nextLine();// this piece of code takes the input of the user for the 4 letter word
		
		String userLetter = reader.nextLine();// this is for the letter guess
		userLetter = userLetter.toUpperCase();// every letter by default is made uppercase

		Hangman.runGame(userLetter);
	}
	else{
		System.out.println("Lauching Wordle... Please wait a moment");
		Wordle.runGame();
	}

	}
}